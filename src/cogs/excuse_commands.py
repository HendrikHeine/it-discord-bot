import discord
from discord import app_commands
from discord import Interaction
from discord.ext import commands
from discord.ext.commands import Cog

import sys
sys.path.append('..')

from src.client import Client as Bot


class ProgrammingExcuses(Cog):
  def __init__(self, bot:Bot):
    self.bot = bot
    self.log = bot.log
    self.programmer_excuses = bot.addons.ProgrammerExcuses()

  @commands.Cog.listener()
  async def on_ready(self):
    self.log.info("Done: excuse")

  @app_commands.command(name="excuse", description="Excuse")
  async def excuse(self, interaction: Interaction):
    self.log.info("Command: excuse")
    await interaction.response.send_message(self.programmer_excuses.get_excuse())


async def setup(bot:Bot):
  await bot.add_cog(ProgrammingExcuses(bot))
  bot.log.info(f"Loaded excuses")

import discord
from discord import app_commands
from discord import Interaction
from discord import Embed
from discord import Color
from discord.ext import commands
from discord.ext.commands import Cog

import sys
sys.path.append('..')

from src.client import Client as Bot


class XkcdCommands(Cog):
  def __init__(self, bot: Bot):
    self.bot = bot
    self.log = bot.log
    self.xkcd = bot.addons.XKCD()

  @commands.Cog.listener()
  async def on_ready(self):
    self.log.info("Done: get-latest-comic, get-random-comic")

  @app_commands.command(name="get-latest-comic", description="test slash command")
  async def get_latest_comic(self, interaction: Interaction):
    self.log.info("Command: get-latest-comic")
    comic = self.xkcd.get_last_comic()

    embed = Embed(title=comic.title, color=Color.blue(), url=f"{self.xkcd.url}/{comic.num}")
    embed.set_image(url=comic.img)
    await interaction.response.send_message(embed=embed)

  @app_commands.command(name='get-random-comic', description='Get a random comic from XKCD')
  async def get_random_comic(self, interaction: Interaction):
    self.log.info("Command: get-random-comic")
    comic = self.xkcd.get_random_comic()

    embed = Embed(title=comic.title, color=Color.blue(), url=f"{self.xkcd.url}/{comic.num}")
    embed.set_image(url=comic.img)
    await interaction.response.send_message(embed=embed)


async def setup(bot):
  await bot.add_cog(XkcdCommands(bot))
  bot.log.info(f"Loaded XKCD")

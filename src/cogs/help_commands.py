import discord
from discord import app_commands
from discord import Interaction
from discord import Embed
from discord import Color
from discord.ext import commands
from discord.ext.commands import Cog

import sys
sys.path.append('..')

from src.client import Client as Bot


class HelpCommands(Cog):
  def __init__(self, bot:Bot):
    self.bot = bot
    self.log = bot.log
    self.programmer_excuses = bot.addons.ProgrammerExcuses()
    self.date_time_helper = bot.addons.DateTimeHelper

  @commands.Cog.listener()
  async def on_ready(self):
    self.log.info("Done: help, info")

  @app_commands.command(name="help", description="List of all Commands")
  async def help(self, interaction: Interaction):
    self.log.info("Command: help")

    command_list_string = ""
    command_list_string += "`/info`            : Get infos about the server and the Bot\n"
    command_list_string += "`/help`            : Get this view\n"
    command_list_string += "`/get-random-comic`: Get a randowm XCCD comic\n"
    command_list_string += "`/get-latest-comic`: Get latest comic from XKCD\n"
    command_list_string += "`/excuse`          : Get a random excuse from programmingexcuses"

    embed = Embed(title=f"Help", description="List of commands", color=Color.blue())
    embed.add_field(name="Commands", value=command_list_string, inline=True)
    await interaction.response.send_message(embed=embed)

  @app_commands.command(name="info", description="Get info about this bot")
  async def info(self, interaction: Interaction):
    self.log.info("Command: info")

    bot_string = ""
    bot_string += f"Uptime    : {await self.bot.get_uptime()}s\n"
    bot_string += f"Version   : {self.bot.version} from {self.bot.date}\n"
    bot_string += f"On Servers: {await self.bot.count_guilds()}\n"
    bot_string += f"Developer : dasmoorhuhn\n"
    bot_string += f"Sourcecode: [Gitlab](https://gitlab.com/DasMoorhuhn/tux-discord-bot)\n"
    bot_string += f"Privacy   : [Read about privacy](https://gitlab.com/DasMoorhuhn/tux-discord-bot/-/blob/main/README.md?ref_type=heads#privacy)"

    embed = Embed(title=f"Info", description="about this Bot",
                  color=Color.blue())
    embed.add_field(name="Bot", value=bot_string, inline=False)
    await interaction.response.send_message(embed=embed)


async def setup(bot:Bot):
  await bot.add_cog(HelpCommands(bot))
  bot.log.info(f"Loaded help")

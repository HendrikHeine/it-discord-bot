import json


def get_bot_token():
  with open(file='secret.json', mode='r') as file:
    secret = json.load(file)
    return secret['BOT_TOKEN']

import requests


class ProgrammerExcuses:
  def __init__(self) -> None:
    self.url = "http://programmingexcuses.com"

  def get_excuse(self):
    page = requests.get(url=self.url)
    content = page.content.decode().split("\n")
    for html in content:
      if 'href="/"' in html:
        start_index = html.find('3;">')
        end_index = html.find("</a></center>")
        return html[start_index + 4:end_index]

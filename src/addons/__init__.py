import sys
sys.path.append('..')

from src.addons.date_time_helper import DateTimeHelper
from src.addons.programmer_excuses import ProgrammerExcuses
from src.addons.xkcd import XKCD

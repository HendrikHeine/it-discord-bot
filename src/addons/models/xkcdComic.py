class Comic:
  def __init__(self, data) -> None:
    self.day = data['day']
    self.month = data['month']
    self.year = data['year']
    self.num = data['num']
    self.link = data['link']
    self.news = data['news']
    self.safe_title = data['safe_title']
    self.transcript = data['transcript']
    self.alt = data['alt']
    self.img = data['img']
    self.title = data['title']

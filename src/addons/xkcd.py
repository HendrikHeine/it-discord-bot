import requests
import json
import sys
sys.path.append('..')

from src.addons.models.xkcdComic import Comic


class XKCD:
  def __init__(self) -> None:
    self.url = "https://xkcd.com"
    self.api = "info.0.json"

  def get_random_comic(self):
    response = requests.get(url=f"https://c.xkcd.com/random/comic")

    for line in response.text.split("\n"):
      if '<meta property="og:url"' in line:
        line = line.split('"')
        for cell in line:
          if "https" in cell:
            return self.get_specific_comic(url=cell)

  def get_last_comic(self):
    response = requests.api.get(f"{self.url}/{self.api}")
    return Comic(json.loads(response.text))

  def get_specific_comic(self, num=None, url=None):
    if num is not None:
      response = requests.api.get(url=f"{self.url}/{num}/{self.api}")
      return Comic(json.loads(response.text))

    if url is not None:
      response = requests.api.get(url=f"{url}/{self.api}")
      return Comic(json.loads(response.text))

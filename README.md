# Tux Bot

This is just a little fun bot. It can give you an excuse, if you're stuck while programming.

## Getting started

Create a file calles `secret.json` in the directory, where the `docker-compose.yml` file is. Put there your discord bot token in:
```json
{
  "BOT_TOKEN": "YOUR_TOKEN"
}
```
And start the discord bot with:

```shell
docker-compose up --build
```
For getting the right invite link with the right permissions, run:

```shell
python3 create_invite_link.py YOUR_BOTS_ID
```
You can get the ID from the developer portal or from the terminal log while starting the discord bot

## Privacy

The discord bot will only log, whenever a command was called and whenever the bot was added or removed from a discord server. It won't log any user/server specific information.